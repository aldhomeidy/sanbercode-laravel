<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function Index()
    {
        $questions = DB::table('questions')->get();
        return view('questions.index', compact('questions'));
    }
    public function Create()
    {
        return view('questions.form_create');
    }
    public function Store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:questions',
            'isi' => 'required',
        ]);
        $query = DB::table('questions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "created_at" => now(),
            "profil_id" => 1 //ini karena saya menggunakan tabel pada tugas migration kemarin, sehingga harus ada profil_id nya
        ]);
        return redirect('/pertanyaan');
    }
    public function Show($id)
    {
        $questions = DB::table('questions')->where('id', $id)->first();
        return view('questions.show', compact('questions'));
    }
    public function Edit($id)
    {
        $questions = DB::table('questions')->where('id', $id)->first();
        return view('questions.form_edit', compact('questions'));
    }
    public function Update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:questions',
            'isi' => 'required',
        ]);

        $query = DB::table('questions')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"],
                'updated_at' => now()
            ]);
        return redirect('/pertanyaan');
    }
    public function Destroy($id)
    {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
