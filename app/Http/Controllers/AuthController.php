<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register_Show()
    {
        return view('signUp');
    }
    public function Register(Request $request)
    {
        $namaLengkap = $request->firstName . " " . $request->lastName;
        return view('welcomeCustom', ['namaLengkap' => $namaLengkap]);
    }
    public function Welcome()
    {
        return view('welcomeCustom');
    }
}
