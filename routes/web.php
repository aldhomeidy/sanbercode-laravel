<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//tugas hari ke 3 week 3
// Route::get('/', function () {
//     return view('items.index');
// });
// Route::get('/data-tables', function () {
//     return view('items.dataTables');
// });

//tugas hari ke 2 week 3
Route::get('/', 'HomeController@Index');
// Route::get('register', 'AuthController@Register_Show');
// Route::post('register/create', 'AuthController@Register');
// Route::get('welcome', 'AuthController@Welcome');


//tugas hari ke 5 week 3
Route::get('/pertanyaan', 'PostController@Index');
Route::get('/pertanyaan/create', 'PostController@Create');
Route::post('/pertanyaan', 'PostController@Store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PostController@Show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PostController@Edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PostController@Update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PostController@Destroy');
