<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 45);
            $table->string('isi', 255);
            $table->timestampsTz(0);

            // $table->int('exactly_answer_id');
            // $table->foreign('exactly_answer_id', 'fk_answer_profil1_idx')
            //     ->references('id')
            //     ->on('profils')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade')
            //     ->index();

            // $table->int('profil_id');
            // $table->foreign('profil_id', 'fk_questions_profil1_idx')
            //     ->references('id')
            //     ->on('profils')
            //     ->onDelete('cascade')
            //     ->onUpdate('cascade')
            //     ->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
