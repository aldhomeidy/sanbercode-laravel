@extends('adminLTE.master')

@section('content')
<div class="card">
    <div class="d-flex card-header">
        <span class="d-block p-2">
            <h3 class="card-title">Questions Lists</h3>
        </span>
        <span class="d-block p-2"> <a href="/pertanyaan/create" class="btn btn-sm btn-primary">Add</a>
        </span>
    </div>
    <div class="card-body">
        <table id="questions" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($questions as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td>{{$value->created_at}}</td>
                    <td>{{$value->updated_at}}</td>
                    <td>
                        <a href="/pertanyaan/{{$value->id}}" class="badge bg-info">Show</a>
                        <a href="/pertanyaan/{{$value->id}}/edit" class="badge bg-primary">Edit</a>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-sm btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr colspan="5">
                    <td>No data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('dataTableScripts')
<script src="{{asset('adminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#questions").DataTable();
    });
</script>

@endpush