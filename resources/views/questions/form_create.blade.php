@extends('adminLTE.master')

@section('content')
<div class="card">
    <div class="d-flex card-header">
        <span class="d-block p-2">
            <h3 class="card-title">Create Questions</h3>
        </span>
        <span class="d-block p-2"> <a href="/pertanyaan" class="btn btn-sm btn-primary">Index</a>
        </span>
    </div>
    <div class="card-body">
        <form method="post" action="/pertanyaan">
            @csrf
            <div class="row mb-3">
                <label for="judul" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="judul" id="judul">
                    @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="isi" class="col-sm-2 col-form-label">Content</label>
                <div class="col-sm-10">
                    <textarea name="isi" class="form-control" id="isi" cols="30" rows="10"></textarea>
                    @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
</div>
@endsection