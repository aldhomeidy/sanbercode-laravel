@extends('adminLTE.master')

@section('content')
<div class="mx-5 py-3">
    <ul class="list-group">
        <li class="list-group-item"><b>Title ID</b></li>
        <li class="list-group-item">{{$questions->id}}</li>
        <li class="list-group-item"><b>Title</b></li>
        <li class="list-group-item">{{$questions->judul}}</li>
        <li class="list-group-item"><b>Content</b></li>
        <li class="list-group-item">{{$questions->isi}}</li>
        <li class="list-group-item"><b>Created At</b></li>
        <li class="list-group-item">{{$questions->created_at}}</li>
        <li class="list-group-item"><b>Updated At</b></li>
        <li class="list-group-item">{{$questions->updated_at}}</li>
    </ul>
    <a href="/pertanyaan">Questions List</a>
</div>
@endsection